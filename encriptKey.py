#- IMPORTATION DU MODULE DE CRYPTOGRAPHIE ET SYSTEM
from cryptography.fernet import Fernet
import os

#- **********************************************
#- FONCTION DE CREATION DE CLE DE CHIFFREMENT
def encryp_key():
    #- GENERE UNE CLE DE CHIFFREMENT
    key = Fernet.generate_key()

    #- PLACER LA CLE DE CHIFFREMENT DANS LE MEME DOSSIER QIE LE FICHIER A CHIFFRER
    with open("unlock.key", "wb") as unlock:
        unlock.write(key)

    #- OUVRIR LA CLE DE CHIFFREMENT A CETTE EMPLACEMENT
    with open("unlock.key", "rb") as unlock:
        key = unlock.read()
        
    return

#- **********************************************
#- FONCTION DE  CHIFFREMENT DU FICHIER
def encrypt(original_file_name, encrypt_file_name):
    #- OUVRIR LA CLE DE CHIFFREMENT A CETTE EMPLACEMENT
    with open("unlock.key", "rb") as unlock:
        key = unlock.read()

    #- UTILISATION DE LA GENERE
    f = Fernet(key)

    #-OUVERTURE DU FICHIER D'ORIGINE A CHIFFRER
    with open(original_file_name, "rb") as original_file:
        original = original_file.read()

    #- CHIFFREMENT DU FICHIER
    encrypted = f.encrypt(original)

    #- ECRITURE DES DONNEES DU FICHIER ENCRYPTE DANS UN FORMAT PROPICE DE FICHIER CHIFFRE
    with open (encrypt_file_name, 'wb') as encrypted_file:
        encrypted_file.write(encrypted)

    #- SUPPRESSION DU FICHIER A CRYPTER
    os.remove(original_file_name)

    return 


#- **********************************************
#- FONCTION DE  DECHIFFREMENT DU FICHIER
def decrypt(encrypt_file_name, decrypt_file_name):
    #- OUVRIR LA CLE DE CHIFFREMENT A CETTE EMPLACEMENT
    with open("unlock.key", 'rb') as unlock:
        key = unlock.read()

    #- UTILISATION DE LA GENERE
    f = Fernet(key)

    #- OUVERTURE DU FICHIER D'ORIGINE CRYPTE
    with open(encrypt_file_name, "rb") as encrypted_file:
        encrypted = encrypted_file.read()

    #- CHIFFREMENT DU FICHIER
    decrypted = f.decrypt(encrypted)

    #- ECRITURE DES DONNEES DU FICHIER DECRYPTE DANS UN FORMAT PROPICE DE FICHIER DECHIFFRE
    with open(decrypt_file_name, "wb") as decrypted_file:
        decrypted_file.write(decrypted)

    #- SUPPRESSION DU FICHIER A CRYPTER
    os.remove(encrypt_file_name)

    return 

encryp_key()
encrypt("FILE_TO_PROTECT.docx", "FILE_TO_PROTECT_ENCRYPTED.docx")
decrypt("FILE_TO_PROTECT_ENCRYPTED.docx", "FILE_TO_PROTECT_DECRYPTED.docx")