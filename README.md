# Encryption Key

## What is an encryption key
A key is a parameter used as input for a cryptographic operation (encryption, decryption, sealing, digital signature, signature verification).

An encryption key can be symmetric (symmetric cryptography) or asymmetric (asymmetric cryptography). In the first case, the same key is used to encrypt and decrypt. In the second case, two different keys are used, the public key is used for encryption while the one used for decryption is kept secret: the secret key, or private key, and cannot be deduced from the public key.

A key can take several forms: words or sentences, procedure for preparing an encryption machine (connections, cabling, etc. See Enigma machine), data coded in binary form (modern cryptology).

## why an encryption key

We will encrypt and decrypt files using Python using a cryptography library. In other words, you are going to secure files using Python. We live in a modern world where data/information transfer is very easy and easy to access. But the secure transfer of data is also important, which is why data security remains one of the main concerns.

## Basics of cryptography

**Encrypt**: Encryption is a process of turning plain text into something meaningless that will appear random (ciphertext).
**Decryption**: Decryption is a process of transforming encrypted files into normal files that we can understand (clear text).

## How to secure files with python and using cryptography?
- Installation of the python cryptography library.
```bash
pip install cryptography
```
- Creation of a key for cryptography.
- Loading the key.
- File encryption.
- File decryption.

**Note** : we can go further by putting a virus in it that will affect the person who will potentially open without having the valid key like for example this famous virus that I have already presented to you : **[CrashComputerVirus](https://gitlab.com/abdelfadil/crash-virus/)** always available and that you can integrate into your encryption key